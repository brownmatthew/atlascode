export enum Time {
    SECONDS = 1000,
    MINUTES = 60000,
    HOURS = 3600000
}